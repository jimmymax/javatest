package cn.mnmcloud.frame;

import cn.mnmcloud.annotation.Controller;
import cn.mnmcloud.helper.BeanHelper;
import cn.mnmcloud.helper.IocHelper;
import cn.mnmcloud.util.ClassHelper;
import cn.mnmcloud.util.ClassUtil;

/**
 * Created by max on 2018/7/31.
 */
public final class HelperLoader {
    public static void init(){
        Class<?>[] classList = {ClassHelper.class, BeanHelper.class, IocHelper.class, Controller.class};
        for(Class<?> cls : classList){
            ClassUtil.loadClass(cls.getName(), true);
        }
    }
}
