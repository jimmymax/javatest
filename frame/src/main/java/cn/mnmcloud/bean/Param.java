package cn.mnmcloud.bean;

import cn.mnmcloud.util.CastUtil;

import java.util.Map;

/**
 * Created by max on 2018/8/1.
 */
public class Param {
    private Map<String, Object> paramMap;

    public  Param(Map<String, Object> paramMap){
        this.paramMap = paramMap;
    }

    public long getLong(String name){
        return CastUtil.castLong(paramMap.get(name));
    }

    public Map<String, Object> getParamMap(){
        return  paramMap;
    }
}
