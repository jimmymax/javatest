package cn.mnmcloud.bean;

/**
 * Created by max on 2018/8/1.
 */
public class Data {
    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
