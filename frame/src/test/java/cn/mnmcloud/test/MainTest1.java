package cn.mnmcloud.test;

/**
 * Created by max on 2018/8/5.
 */
public class MainTest1 {
    public static void main(String [] args){
        Hello helloProxy = new HelloProxy();
        helloProxy.sayHello("JACK1");

        Hello hello = new HelloImp();
        DynamicProxy dynamicProxy = new DynamicProxy(hello);
        Hello helloProxy1 = dynamicProxy.getProxy();
        helloProxy1.sayHello("jack2");

        Hello helloProxy2 = CGLibProxy.getInstance().getProxy(HelloImp.class);
        helloProxy2.sayHello("jack3");

        HelloImp helloProxy3 = CGLibProxy.getSProxy(HelloImp.class);
        helloProxy3.sayHello("jack4");
        helloProxy1.sayHello("jack");

        Hello helloProxy2 = CGLibProxy.getInstance().getProxy(HelloImp.class);
        helloProxy2.sayHello("jack3");
    }
}
