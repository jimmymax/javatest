package com.mtel.scheduler;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.mtel.scheduler")
@MapperScan("com.mtel.scheduler.dao")
@SpringBootApplication
public class SchedulerApplication {

    public static void main(String[] args) {

        SpringApplication.run(SchedulerApplication.class, args);
    }
}
