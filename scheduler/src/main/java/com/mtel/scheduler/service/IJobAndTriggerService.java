package com.mtel.scheduler.service;

import com.mtel.scheduler.entity.JobAndTrigger;
import com.github.pagehelper.PageInfo;

/**
 * Created by max on 2018/7/18.
 */
public interface IJobAndTriggerService {
    PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize);
}
