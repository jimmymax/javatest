package com.mtel.scheduler.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mtel.scheduler.dao.JobAndTriggerMapper;
import com.mtel.scheduler.entity.JobAndTrigger;
import com.mtel.scheduler.service.IJobAndTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by max on 2018/7/18.
 */
@Service
public class JobAndTriggerImlp implements IJobAndTriggerService{
    @Autowired
    private JobAndTriggerMapper jobAndTriggerMapper;

    @Override
    public PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
        PageInfo<JobAndTrigger> page = new PageInfo<>(list);
        return page;
    }

    //    public PageInfo<JobAndTrigger> getJobAndTrigerDetails(int pageNum, int pageSize) {
//        PageHelper.startPage(pageNum, pageSize);
//        List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
//        PageInfo<JobAndTrigger> page = new PageInfo<JobAndTrigger>(list);
//        return page;
//    }
}
