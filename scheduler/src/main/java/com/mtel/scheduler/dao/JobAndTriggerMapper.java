package com.mtel.scheduler.dao;

import com.mtel.scheduler.entity.JobAndTrigger;

import java.util.List;

/**
 * Created by max on 2018/7/18.
 */
public interface JobAndTriggerMapper {
    public List<JobAndTrigger> getJobAndTriggerDetails();
}
