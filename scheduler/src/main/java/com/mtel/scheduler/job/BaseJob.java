package com.mtel.scheduler.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by max on 2018/7/18.
 */
public interface BaseJob extends Job {
    void execute(JobExecutionContext context) throws JobExecutionException;
}
