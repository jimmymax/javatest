package com.mtel.scheduler.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by max on 2018/7/18.
 */
public class NewJob implements BaseJob{
    private  static Logger _log = LoggerFactory.getLogger(NewJob.class);

    public NewJob(){}

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        _log.error("New Job execution time:" + new Date());
    }
}
