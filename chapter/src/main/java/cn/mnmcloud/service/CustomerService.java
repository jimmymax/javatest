package cn.mnmcloud.service;

import cn.mnmcloud.helper.DatabaseHelper;
import cn.mnmcloud.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


/**
 *
 * @author max
 * @date 2018/7/10
 */
public class CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);


    public  List<Customer> getCustomerList(){
//        Connection conn = null;
//        try {
//            List<Customer> customerList = new ArrayList<Customer>();
//            String sql = "select * from customer";
//            conn= DatabaseHelper.getConnection();
//            PreparedStatement pstmt = conn.prepareStatement(sql);
//            ResultSet rs = pstmt.executeQuery();
//            while(rs.next()){
//                Customer customer = new Customer();
//                customer.setId(rs.getLong("id"));
//                customer.setName(rs.getString("name"));
//                customer.setContract(rs.getString("contact"));
//                customer.setTelephone(rs.getString("telephone"));
//                customer.setEmail(rs.getString("email"));
//                customer.setRemark(rs.getString("remark"));
//                customerList.add(customer);
//            }
//            return  customerList;
//        } catch (SQLException e) {
//            LOGGER.error("execute sql failure", e);
//        } finally {
//            DatabaseHelper.closeConnection(conn);
//        }
//        return  null;
        String sql="select * from Customer";
        return DatabaseHelper.queryEntityList(Customer.class,sql);
    }

    public  List<Customer> getCustomerList(String keyword){
        String sql = "SELECT * FROM customer WHERE name like '%?%'";
        return DatabaseHelper.queryEntityList(Customer.class, sql, keyword);
    }

    public  Customer getCustomer(long id){
        String sql = "SELECT * FROM customer WHERE id = ?";
        return DatabaseHelper.queryEntity(Customer.class, sql, id);
    }

    public  boolean createCustomer(Map<String, Object> fieldMap){
        return DatabaseHelper.insertEntity(Customer.class,fieldMap);
    }

    public  boolean updateCustomer(long id, Map<String, Object> fieldMap){
        return DatabaseHelper.updateEntity(Customer.class,id,fieldMap);
    }

    public  boolean deleteCustomer(long id){
        return DatabaseHelper.deleteEntity(Customer.class,id);
    }
}
