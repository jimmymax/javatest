package cn.mnmcloud.model;

/**
 *
 * @author max
 * @date 2018/7/9
 * Customer
 */
public class Customer {
    /**
     * ID
     */
    private  long id;

    /**
     * name
     */
    private  String name;

    /**
     * contract
     */
    private String contract;

    /**
     * Phone
     */
    private String telephone;

    /**
     * Email
     */
    private String email;

    /**
     * remark
     */
    private String remark;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contract;
    }

    public void setContact(String contract) {
        this.contract = contract;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
