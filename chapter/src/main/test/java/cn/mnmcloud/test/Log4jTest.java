package cn.mnmcloud.test;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log4jTest {
//    private  static Logger logger = Logger.getLogger(Log4jTest.class.getName());
//
//    public static void main(String[] args) {
//        // 记录debug级别的信息
//        logger.debug("This is debug message.");
//        // 记录info级别的信息
//        logger.info("This is info message.");
//        // 记录error级别的信息
//        logger.error("This is error message.");
//
//    }

    private static final Logger log = LoggerFactory.getLogger("MainLogger");

    @Test
    public void test(){
        boolean logWork = true;
        try {
            log.info("info...");
            log.debug("debug...");
            log.warn("warn...");
            log.error("error...");
        }catch (Exception ex){
            logWork = false;
        }
        Assert.assertTrue(logWork);
    }
}
